#!/bin/bash
if [ $# -ne 1 ] 
then
	echo "只带一个参数"
	exit
fi
mkdir -p $PWD/$1/{tasks,files,templates,meta,handlers,vars}
touch $PWD/$1/{tasks/main.yml,meta/main.yml,handlers/main.yml,vars/main.yml}

cat << EOF > $PWD/$1/$1.yml
---
- name: Test playbook
  hosts: all
  roles:
    - $1
EOF
