# ansible-k8s-role

#### 介绍
ansible安装单点k8s-1.14集群role

#### 软件架构
软件架构说明
集群版本：k8s-1.14  
网络组件：calico-ipip（默认）  
默认安装组件：coredns、calico、dashboard、helm、ingress-controller  

#### 安装教程

1.  ansible 主机配置K8S集群所有节点主机的免秘钥登录
2.  ansible-playbook k8s-install.yml 即可完成k8s master、etcd单点集群搭建
3.  参考hosts文件配置参数，注意k8s集群节点私有网段跟service-ip-range和pod-ip-range冲突问题
4.  遇到错误中断，可以重新跑，后续处理下错误处理  
5.  k8s主机节点配置如下
![输入图片说明](https://images.gitee.com/uploads/images/2019/1119/163833_c7561485_5237999.png "屏幕截图.png")
6.证书内部安全，外网无法通过证书访问k8s集群，生产环境请自行更换证书  

ansible安装  

yum install -y ansible  
下载到/etc/ansible/roles目录下,  
ansible-playbook -i hosts k8s-install.yml  
![输入图片说明](https://images.gitee.com/uploads/images/2019/1119/172347_b17a97ee_5237999.png "屏幕截图.png")

